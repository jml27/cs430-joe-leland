from .Cartdbmodels import ReviewsModel
import sqlite3
import time
import random
DB_FILE = 'foodcartreviews.db'


class reviewModel(ReviewsModel):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from reviews")
        except sqlite3.OperationalError:
            cursor.execute(
                "create table reviews (id text PRIMARY KEY, name text, rating integer, text text, time_created text, url text, cart_id text, FOREIGN KEY(cart_id) REFERENCES foodcarts(id))")
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, email, date, message
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM reviews")
        return cursor.fetchall()

    def selectSome(self, queryID):
        """
        Gets all rows from the database
        Each row contains: name, email, date, message
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute(
            "SELECT * FROM reviews WHERE cart_id='{}'".format(queryID))
        return cursor.fetchall()

    def insert(self, id, name, rating, text, time_created, url, cart_id):
        """
        Inserts entry into database
        :param id: String
        :param name: String
        :param rating: Int
        :param text: String
        :param time_created: String
        :param url: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'id': id, 'name': name, 'rating': rating, 'text': text,
                  'time_created': time_created, 'url': url, 'cart_id': cart_id}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute(
            "insert or replace into reviews (id, name, rating, text, time_created, url, cart_id) VALUES (:id, :name, :rating, :text, :time_created, :url, :cart_id)", params)

        connection.commit()
        cursor.close()
        return True
