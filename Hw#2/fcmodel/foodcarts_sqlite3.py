from .Cartdbmodels import FoodCartModel
import sqlite3
DB_FILE = 'foodcartreviews.db'


class cartModel(FoodCartModel):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from foodcarts")
        except sqlite3.OperationalError:
            cursor.execute("create table foodcarts (id text PRIMARY KEY, name text, phone text, rating integer, review_count integer, url text, image_url text, is_closed integer, distance integer, street text, city text, state text, zip_code text, latitude, longitude)")
        cursor.execute("PRAGMA foreign_keys=ON;")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, email, date, message
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM foodcarts")
        return cursor.fetchall()

    def updateReviewCount(self, matchId):
        """
         update priority, begin_date, and end date of a task
         :param matchId: String
         :return: cart id
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute(
            "UPDATE foodcarts SET review_count= review_count + 1 WHERE id=?", (matchId,))
        connection.commit()

    def insert(self, id, name, phone, rating, review_count, url, image_url, is_closed, distance, street, city, state, zip_code, latitude, longitude):
        """
        Inserts entry into database
        :param name: String
        :param phone: String
        :param rating: Int
        :param review_count: Int
        :param reviews: lookinto best way to do this. will be an array of review objects, foreign key to other table?
        :param url: String
        :param location: location object, foreign key?
        :param image_url: String
        :param is_closed: Boolean
        :param id: String
        :Param distance: Int
        :param street: String
        :param city: String
        :param state: String
        :param zip: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'id': id, 'name': name, 'phone': phone, 'rating': rating, 'review_count': review_count, 'url': url, 'image_url': image_url, 'is_closed': is_closed,
                  'distance': distance, 'street': street, 'city': city, 'state': state, 'zip_code': zip_code, 'latitude': latitude, 'longitude': longitude}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert or replace into foodcarts ( id, name, phone, rating, review_count, url, image_url, is_closed, distance, street, city, state, zip_code, latitude, longitude) VALUES (:id, :name, :phone, :rating, :review_count, :url, :image_url, :is_closed, :distance, :street, :city, :state, :zip_code, :latitude, :longitude)", params)

        connection.commit()
        cursor.close()
        return True
